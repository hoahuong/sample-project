<%@ page language="java" contentType="text/html; charset=utf8"
	pageEncoding="utf8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>TOP</title>

<link
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
	rel='stylesheet' type='text/css' />
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>

<body>
	<nav class="navbar navbar-toggleable-md fixed-top"> </nav>
	<div class="container">
		<div class="col-md-4">
			<form action="${pageContext.request.contextPath}/top" method='POST'>
				<div class="form-group">
					<textarea class="form-control" name="keyword"
						placeholder="Input Keyword Here"></textarea>
				</div>
				<div class="form-group">
					<button type="submit" class="btn btn-default center-block">Regist</button>
				</div>
			</form>
		</div>

		<div class="col-md-8">
			<table class="table table-bordered">
				<thead>
					<tr>
						<th>ID</th>
						<th>Keyword</th>
						<th>Status</th>
						<th>Date</th>
					</tr>
				</thead>
				<tbody>
					<c:if test="${keywordList.size() == 0}">
						<tr>
							<td colspan="4" class="text-center">No data</td>
						</tr>
					</c:if>
					<c:forEach items="${keywordList}" var="keyword">
						<tr>
							<td>${keyword.id}</td>
							<td>${keyword.keyword}</td>
							<c:if test="${keyword.statusId == 3}">
								<td><a href="url_list?kwdId=${keyword.id}"
									style="color: blue;">${keyword.status}</a></td>
							</c:if>
							<c:if test="${keyword.statusId == 2 || keyword.statusId == 1}">
								<td style="color: red;">${keyword.status}</td>
							</c:if>
							<c:if test="${keyword.statusId == 0}">
								<td style="color: green;">${keyword.status}</td>
							</c:if>
							<td>${keyword.inputUpdate}</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
	</div>
	<!-- /.container -->
</body>
</html>