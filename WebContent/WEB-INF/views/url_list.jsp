<%@ page language="java" contentType="text/html; charset=utf8"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf8">
<title>Result list of searching from Google</title>
<link
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
	rel='stylesheet' type='text/css' />
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
	<nav class="navbar navbar-toggleable-md fixed-top"> </nav>
	<div class="container">
		<table class="table table-bordered">
			<thead>
				<tr>
					<th>Rank</th>
					<th>Title</th>
					<th>URL</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${urlList}" var="urlInfo">
					<tr>
						<td>${urlInfo.rank}</td>
						<td>${urlInfo.title}</td>
						<td><a href="anchor_detail?urlId=${urlInfo.id}&kwdId=${urlInfo.kwdId}&rank=${urlInfo.rank}&page=0">${urlInfo.url}</a></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>
	<!-- /.container -->
</body>
</html>