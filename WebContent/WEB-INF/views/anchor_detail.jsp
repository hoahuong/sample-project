<%@ page language="java" contentType="text/html; charset=utf8"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf8">
<title>Anchor Detail Page</title>
<link
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
	rel='stylesheet' type='text/css' />
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
	<!-- <nav class="navbar navbar-toggleable-md fixed-top"> </nav> -->
	<div class="panel panel-default">
		<div class="panel-body">
			URL: <a href="${curUri}">${curUri}</a>
		</div>
	</div>
	<div class="container">
		<div class="clearfix"></div>
		<div class="row">
			<div class="col-xs-12">
				<table class="table table-bordered">
					<tbody>
						<tr>
							<th>Title</th>
							<td>${pTitle}</td>
						</tr>
						<tr>
							<th>Meta description</th>
							<td>${pDesc}</td>
						</tr>
					</tbody>
				</table>
				<table class="table table-bordered">
					<thead>
						<tr>
							<th>ID</th>
							<th>Anchor Text</th>
							<th>Anchor Type</th>
							<th>Anchor URL</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${adList}" var="anchorDetail">
							<tr>
								<td>${anchorDetail.id}</td>
								<td>${anchorDetail.anchorText}</td>
								<td>${anchorDetail.anchorType}</td>
								<td style="word-break: break-all"><a href="${anchorDetail.anchorUrl}">${anchorDetail.anchorUrl}</a></td>
							</tr>
						</c:forEach>
						<c:if test="${adList.size() == 0}">
							<tr>
								<td colspan="4" class="text-center">No data</td>
							</tr>
						</c:if>
					</tbody>
				</table>
				<%-- <ul class="pagination no-margin pull-right">
					<c:forEach begin="0" end="${allPageNum}" var="pageNum">
						<c:choose>
							<c:when test="${pageNum == curPage}">
								<li class="active"><a
									href="${pageContext.request.contextPath}/anchor_detail?urlId=${urlId}&page=${pageNum}">${pageNum}</a></li>
							</c:when>
							<c:otherwise>
								<li><a
									href="${pageContext.request.contextPath}/anchor_detail?urlId=${urlId}&page=${pageNum}">${pageNum}</a></li>
							</c:otherwise>
						</c:choose>
					</c:forEach>
				</ul> --%>

				<ul class="pagination no-margin pull-right">
					<c:if test="${curPage > 0}">
						<li><a
							href="${pageContext.request.contextPath}/anchor_detail?urlId=${urlId}&page=0">&lt;&lt;</a></li>
						<li><a
							href="${pageContext.request.contextPath}/anchor_detail?urlId=${urlId}&page=${curPage - 1}">&lt;</a></li>
					</c:if>
					<li class="active"><a>${curPage+1}/${allPageNum+1}</a></li>
					<c:if test="${curPage < allPageNum}">
						<li><a
							href="${pageContext.request.contextPath}/anchor_detail?urlId=${urlId}&page=${curPage + 1}">&gt;</a></li>
						<li><a
							href="${pageContext.request.contextPath}/anchor_detail?urlId=${urlId}&page=${allPageNum}">&gt;&gt;</a></li>
					</c:if>
				</ul>
			</div>
		</div>
	</div>
	<!-- /.container -->
</body>
</html>