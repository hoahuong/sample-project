import fss.intern.researcher.anchor.dao.AnchorDetailDAOImpl;
import fss.intern.researcher.anchor.dao.KeywordDAOImpl;
import fss.intern.researcher.anchor.dao.UrlListDAOImpl;
import fss.intern.researcher.anchor.services.AnchorDetailService;

/**
 * @author Huong
 * This class can run by command prompt or java application to get content of link or image from website
 */
public class GetAnchorDetail {

	public static void main(String[] args) {

		System.out.println("I just test run GetAnchorDetail by command prompt! :)");

		AnchorDetailService ads = new AnchorDetailService(new UrlListDAOImpl(), new AnchorDetailDAOImpl(), new KeywordDAOImpl());
		ads.getAllAnchorDetailFromNet();
	}

}
