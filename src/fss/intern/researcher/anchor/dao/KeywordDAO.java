package fss.intern.researcher.anchor.dao;

import java.util.List;
import fss.intern.researcher.anchor.entities.Keyword;

public interface KeywordDAO {
	public Keyword getKeyword(int kwId);
	public List<Keyword> getKeywordList();
	public boolean insert(Keyword kwd);
	public void updateStatus(Keyword kwd, int status);
	public void updateStatus(int kwdId, int status);
}
