package fss.intern.researcher.anchor.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import fss.intern.researcher.anchor.entities.AnchorDetail;
import fss.intern.researcher.anchor.entities.PageInfo;
import fss.intern.researcher.anchor.utils.MySQLConnUtils;

public class AnchorDetailDAOImpl implements AnchorDetailDAO {

	public PageInfo getPageInfo(int urlId) {
		PageInfo pInfo = null;
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			conn = MySQLConnUtils.getMySQLConnection();
			ps = conn.prepareStatement("Select * from page where url_id = " + urlId);
			rs = ps.executeQuery();
			try {
				while (rs.next()) {
					String title = rs.getString("title");
					String desc = rs.getString("description");
					pInfo = new PageInfo(title, desc, urlId);
				}
			} finally {
				rs.close();
			}
		} catch (ClassNotFoundException e) {
			System.out.println(e.getMessage());
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			if (ps != null) {
				try {
					ps.close();
				} catch (SQLException e) {
					System.out.println(e.getMessage());
				}
			}
		}
		return pInfo;
	}

	@Override
	public List<AnchorDetail> getAnchorDetail(int urlId) {
		List<AnchorDetail> anchorDetails = new ArrayList<AnchorDetail>();
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			conn = MySQLConnUtils.getMySQLConnection();
			ps = conn.prepareStatement("Select id, title, type, url from anchor where url_id = " + urlId);
			rs = ps.executeQuery();
			try {
				while (rs.next()) {
					Integer id = rs.getInt("id");
					String title = rs.getString("title");
					String type = rs.getString("type");
					String url = rs.getString("url");
					AnchorDetail anchorDetail = new AnchorDetail(id, title, type, url, urlId);
					anchorDetails.add(anchorDetail);
				}
			} finally {
				rs.close();
			}
		} catch (ClassNotFoundException e) {
			System.out.println(e.getMessage());
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			if (ps != null) {
				try {
					ps.close();
				} catch (SQLException e) {
					System.out.println(e.getMessage());
				}
			}
		}
		return anchorDetails;
	}

	@Override
	public boolean insert(AnchorDetail anchorDetail) {
		Connection conn = null;
		PreparedStatement ps = null;
		try {
			conn = MySQLConnUtils.getMySQLConnection();
			conn.setAutoCommit(false);
			ps = conn.prepareStatement("insert into anchor(title, type, url, url_id) values(?,?,?,?)",
					Statement.RETURN_GENERATED_KEYS);
			ps.setString(1, anchorDetail.getAnchorText());
			ps.setString(2, anchorDetail.getAnchorType());
			ps.setString(3, anchorDetail.getAnchorUrl());
			ps.setInt(4, anchorDetail.getUrlId());
			int result = ps.executeUpdate();
			conn.commit();
			if (result > 0) {
				return true;
			}
		} catch (ClassNotFoundException e) {
			System.out.println(e.getMessage());
			try {
				conn.rollback();
			} catch (SQLException e1) {
				System.out.println(e1.getMessage());
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			if (ps != null) {
				try {
					ps.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if(conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return false;
	}

	public boolean insertPageInfo(PageInfo pInfo) {
		Connection conn = null;
		PreparedStatement ps = null;
		try {
			conn = MySQLConnUtils.getMySQLConnection();
			conn.setAutoCommit(false);
			ps = conn.prepareStatement("insert into page(title, description, url_id) values(?,?,?)",
					Statement.RETURN_GENERATED_KEYS);
			ps.setString(1, pInfo.getTitle());
			ps.setString(2, pInfo.getMetaDesc());
			ps.setInt(3, pInfo.getUrlId());
			int result = ps.executeUpdate();
			conn.commit();
			if (result > 0) {
				return true;
			}
		} catch (ClassNotFoundException e) {
			System.out.println(e.getMessage());
			try {
				conn.rollback();
			} catch (SQLException e1) {
				System.out.println(e1.getMessage());
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			if (ps != null) {
				try {
					ps.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if(conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return false;
	}

	@Override
	public List<AnchorDetail> getAnchorDetailByLimit(int urlId, int from, int limit) {
		List<AnchorDetail> anchorDetails = new ArrayList<AnchorDetail>();
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			conn = MySQLConnUtils.getMySQLConnection();
			ps = conn
					.prepareStatement("Select * from anchor where url_id = " + urlId + " limit " + from + ", " + limit);
			rs = ps.executeQuery();
			try {
				while (rs.next()) {
					Integer id = rs.getInt("id");
					String title = rs.getString("title");
					String type = rs.getString("type");
					String url = rs.getString("url");
					AnchorDetail anchorDetail = new AnchorDetail(id, title, type, url, urlId);
					anchorDetails.add(anchorDetail);
				}
			} finally {
				rs.close();
			}
		} catch (ClassNotFoundException e) {
			System.out.println(e.getMessage());
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			if (ps != null) {
				try {
					ps.close();
				} catch (SQLException e) {
					System.out.println(e.getMessage());
				}
			}
		}
		return anchorDetails;
	}

}
