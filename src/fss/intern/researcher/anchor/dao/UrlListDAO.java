package fss.intern.researcher.anchor.dao;

import java.util.List;

import fss.intern.researcher.anchor.entities.UrlInfo;

public interface UrlListDAO {
//	public String getUri(int keywordId, int rank);
	public List<UrlInfo> getUrlListByKwdId(int keywordId);
	public String getUriById(int id);
	public List<UrlInfo> getUrlListAll();
	public boolean insert(UrlInfo urlInfo);
	public void updateStatus(int urlId, int status);
}
