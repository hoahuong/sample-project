package fss.intern.researcher.anchor.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import fss.intern.researcher.anchor.entities.Keyword;
import fss.intern.researcher.anchor.utils.MySQLConnUtils;

public class KeywordDAOImpl implements KeywordDAO {

	@Override
	public List<Keyword> getKeywordList() {
		List<Keyword> kwdList = new ArrayList<Keyword>();
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			conn = MySQLConnUtils.getMySQLConnection();
			ps = conn.prepareStatement("Select * from keyword");
			rs = ps.executeQuery();
			try {
				while (rs.next()) {
					Integer id = rs.getInt("id");
					String keyword = rs.getString("keyword");
					String status = "";
					Integer statusID = rs.getInt("status");
					switch (statusID) {
					case 0:
						status = "To do";
						break;
					case 1:
						status = "In Progress: Url List generate";
						break;
					case 2:
						status = "In Progress: Anchor generate";
						break;
					case 3:
						status = "Done";
						break;
					default:
						break;
					}
					Date inputDate = rs.getDate("date");
					Keyword kwd = new Keyword(id, keyword, status, statusID, inputDate);
					kwdList.add(kwd);
				}
			} finally {
				rs.close();
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} catch (ClassNotFoundException e) {
			System.out.println(e.getMessage());
		} finally {
			if (ps != null) {
				try {
					ps.close();
				} catch (SQLException e) {
					System.out.println(e.getMessage());
				}
			}
		}

		return kwdList;
	}

	@Override
	public boolean insert(Keyword kwd) {
		Connection conn = null;
		PreparedStatement ps = null;
		try {
			conn = MySQLConnUtils.getMySQLConnection();
			conn.setAutoCommit(false);
			ps = conn.prepareStatement("insert into keyword(keyword, status, date) values(?,?,?)",
					Statement.RETURN_GENERATED_KEYS);
			ps.setString(1, kwd.getKeyword());
			ps.setInt(2, kwd.getStatusId());
			ps.setDate(3, kwd.getInputUpdate());
			int result = ps.executeUpdate();
			// ResultSet rs = ps.getGeneratedKeys();
			// if (rs.next()) {
			// setIdKwdInserted(rs.getInt(1));
			// }
			conn.commit();
			if (result > 0) {
				return true;
			}
		} catch (ClassNotFoundException e) {
			System.out.println(e.getMessage());
			try {
				conn.rollback();
			} catch (SQLException e1) {
				System.out.println(e1.getMessage());
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			if (ps != null) {
				try {
					ps.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if(conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return false;
	}

	@Override
	public void updateStatus(Keyword kwd, int status) {
		Connection conn = null;
		PreparedStatement ps = null;
		try {
			conn = MySQLConnUtils.getMySQLConnection();
			conn.setAutoCommit(false);
			ps = conn.prepareStatement("Update keyword Set status = ? where id = " + kwd.getId());
			ps.setInt(1, status);
			ps.executeUpdate();
			conn.commit();
		} catch (ClassNotFoundException e) {
			System.out.println(e.getMessage());
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			if (ps != null) {
				try {
					ps.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if(conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}

	}

	public void updateStatus(int kwdId, int status) {
		Connection conn = null;
		PreparedStatement ps = null;
		try {
			conn = MySQLConnUtils.getMySQLConnection();
			conn.setAutoCommit(false);
			ps = conn.prepareStatement("Update keyword Set status = ? where id =" + kwdId);
			ps.setInt(1, status);
			ps.executeUpdate();
			conn.commit();
		} catch (ClassNotFoundException e) {
			System.out.println(e.getMessage());
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			if (ps != null) {
				try {
					ps.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if(conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}

	}

	@Override
	public Keyword getKeyword(int kwId) {
		// TODO Auto-generated method stub
		Keyword kwd = null;
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			conn = MySQLConnUtils.getMySQLConnection();
			ps = conn.prepareStatement("Select * from keyword where id = " + kwId);
			rs = ps.executeQuery();
			try {
				while (rs.next()) {
					Integer id = rs.getInt("id");
					String keyword = rs.getString("keyword");
					String status = "";
					Integer statusID = rs.getInt("status");
					Date d = rs.getDate("date");
					kwd = new Keyword(id, keyword, status, statusID, d);
				}
			} finally {
				rs.close();
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} catch (ClassNotFoundException e) {
			System.out.println(e.getMessage());
		} finally {
			if (ps != null) {
				try {
					ps.close();
				} catch (SQLException e) {
					System.out.println(e.getMessage());
				}
			}
		}

		return kwd;
	}

}
