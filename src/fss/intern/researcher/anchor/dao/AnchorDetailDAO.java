package fss.intern.researcher.anchor.dao;

import java.util.List;

import fss.intern.researcher.anchor.entities.AnchorDetail;
import fss.intern.researcher.anchor.entities.PageInfo;

public interface AnchorDetailDAO {
	public PageInfo getPageInfo(int urlId);
	public List<AnchorDetail> getAnchorDetailByLimit(int urlId, int from, int limit);
	public List<AnchorDetail> getAnchorDetail(int urlId);
	public boolean insert(AnchorDetail anchorDetail);
	public boolean insertPageInfo(PageInfo pInfo);
}
