package fss.intern.researcher.anchor.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import fss.intern.researcher.anchor.entities.UrlInfo;
import fss.intern.researcher.anchor.utils.MySQLConnUtils;

public class UrlListDAOImpl implements UrlListDAO {

	@Override
	public List<UrlInfo> getUrlListByKwdId(int keywordID) {
		List<UrlInfo> urlList = new ArrayList<UrlInfo>();
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			conn = MySQLConnUtils.getMySQLConnection();
			ps = conn.prepareStatement("Select * from url where keyword_id = " + keywordID);
			rs = ps.executeQuery();
			try {
				while (rs.next()) {
					Integer id = rs.getInt("id");
					Integer rank = rs.getInt("rank");
					String title = rs.getString("title");
					String url = rs.getString("uri");
					Integer status = rs.getInt("status");
					UrlInfo urlInfo = new UrlInfo(id, rank, title, url, keywordID, status);
					urlList.add(urlInfo);
				}
			} finally {
				rs.close();
			}
		} catch (ClassNotFoundException e) {
			System.out.println(e.getMessage());
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			if (ps != null) {
				try {
					ps.close();
				} catch (SQLException e) {
					System.out.println(e.getMessage());
				}
			}
			if(conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return urlList;
	}

	@Override
	public String getUriById(int id) {
		String uri = "";
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			conn = MySQLConnUtils.getMySQLConnection();
			ps = conn.prepareStatement("Select uri from url where id = " + id);
			rs = ps.executeQuery();
			try {
				while (rs.next()) {
					uri = rs.getString("uri");
				}
			} finally {
				rs.close();
			}
		} catch (ClassNotFoundException e) {
			System.out.println(e.getMessage());
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			if (ps != null) {
				try {
					ps.close();
				} catch (SQLException e) {
					System.out.println(e.getMessage());
				}
			}
		}
		return uri;
	}

	@Override
	public List<UrlInfo> getUrlListAll() {
		// TODO Auto-generated method stub
		List<UrlInfo> urlList = new ArrayList<UrlInfo>();
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			conn = MySQLConnUtils.getMySQLConnection();
			ps = conn.prepareStatement("Select * from url");
			rs = ps.executeQuery();
			try {
				while (rs.next()) {
					Integer id = rs.getInt("id");
					Integer keywordId = rs.getInt("keyword_id");
					Integer rank = rs.getInt("rank");
					String title = rs.getString("title");
					String url = rs.getString("uri");
					Integer status = rs.getInt("status");
					UrlInfo urlInfo = new UrlInfo(id, rank, title, url, keywordId, status);
					urlList.add(urlInfo);
				}
			} finally {
				rs.close();
			}
		} catch (ClassNotFoundException e) {
			System.out.println(e.getMessage());
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			if (ps != null) {
				try {
					ps.close();
				} catch (SQLException e) {
					System.out.println(e.getMessage());
				}
			}
		}
		return urlList;
	}
	
	@Override
	public boolean insert(UrlInfo urlInfo) {
		Connection conn = null;
		PreparedStatement ps = null;
		try {
			conn = MySQLConnUtils.getMySQLConnection();
			conn.setAutoCommit(false);
			ps = conn.prepareStatement("insert into url(rank, title, uri, keyword_id, status) values(?,?,?,?,?)",
					Statement.RETURN_GENERATED_KEYS);
			ps.setInt(1, urlInfo.getRank());
			ps.setString(2, urlInfo.getTitle());
			ps.setString(3, urlInfo.getUrl());
			ps.setInt(4, urlInfo.getKwdId());
			ps.setInt(5, urlInfo.getStatus());
			int result = ps.executeUpdate();
			conn.commit();
			if (result > 0) {
				return true;
			}
		} catch (ClassNotFoundException e) {
			System.out.println(e.getMessage());
			try {
				conn.rollback();
			} catch (SQLException e1) {
				System.out.println(e1.getMessage());
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			if (ps != null) {
				try {
					ps.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if(conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return false;
	}

	@Override
	public void updateStatus(int urlId, int status) {
		Connection conn = null;
		PreparedStatement ps = null;
		try {
			conn = MySQLConnUtils.getMySQLConnection();
			conn.setAutoCommit(false);
			ps = conn.prepareStatement("Update url Set status = ? where id =" + urlId);
			ps.setInt(1, status);
			ps.executeUpdate();
			conn.commit();
		} catch (ClassNotFoundException e) {
			System.out.println(e.getMessage());
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			if (ps != null) {
				try {
					ps.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if(conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}

	}

}
