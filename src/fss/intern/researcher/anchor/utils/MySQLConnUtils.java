/**
 * 
 */
package fss.intern.researcher.anchor.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * @author Huong
 * connecting to db
 */
public class MySQLConnUtils {
	
	private static Connection conn = null;
	
	private MySQLConnUtils() {}
	
	public static Connection getMySQLConnection() throws ClassNotFoundException, SQLException {

		String hostName = "localhost";
		String dbName = "anchor_researcher";
		String userName = "root";
		String password = "";
		return getMySQLConnection(hostName, dbName, userName, password);
	}

	public static Connection getMySQLConnection(String hostName, String dbName, String userName, String password)
			throws SQLException, ClassNotFoundException {

		if((conn == null) || (conn.isClosed())) {
			Class.forName("com.mysql.jdbc.Driver");

			String connectionURL = "jdbc:mysql://" + hostName + ":3306/" + dbName + "?useUnicode=true&characterEncoding=utf8&charactetrResultSets=utf8&autoReconnect=true";

			conn = DriverManager.getConnection(connectionURL, userName, password);
			
			return conn;
		}
		
		return conn;
	}
}
