package fss.intern.researcher.anchor.entities;

import java.sql.Date;

/**
 * @author Huong mapping object class with keyword table
 */
public class Keyword {

	private Integer id;
	private String keyword;
	private String status;
	private Integer statusId;
	private Date inputUpdate;

	public final static int TO_DO = 0;
	public final static int IN_PROGRESS_GET_LINK_LIST = 1;
	public final static int IN_PROGRESS_GET_ANCHOR = 2;
	public final static int DONE = 3;

	public Keyword(Integer id, String keyword, String status, Integer statusId, Date inputUpdate) {
		super();
		this.id = id;
		this.keyword = keyword;
		this.status = status;
		this.statusId = statusId;
		this.inputUpdate = inputUpdate;
	}

	public Keyword(Integer id, String keyword, Integer statusId, Date inputUpdate) {
		super();
		this.id = id;
		this.keyword = keyword;
		this.statusId = statusId;
		this.inputUpdate = inputUpdate;
	}

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the kwd
	 */
	public String getKeyword() {
		return keyword;
	}

	/**
	 * @param kwd
	 *            the kwd to set
	 */
	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the statusId
	 */
	public Integer getStatusId() {
		return statusId;
	}

	/**
	 * @param statusId
	 *            the statusId to set
	 */
	public void setStatusId(Integer statusId) {
		this.statusId = statusId;
	}

	/**
	 * @return the inputUpdate
	 */
	public Date getInputUpdate() {
		return inputUpdate;
	}

	/**
	 * @param inputUpdate
	 *            the inputUpdate to set
	 */
	public void setInputUpdate(Date inputUpdate) {
		this.inputUpdate = inputUpdate;
	}

}
