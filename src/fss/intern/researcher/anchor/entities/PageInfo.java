package fss.intern.researcher.anchor.entities;

/**
 * @author Huong mapping object class with page table
 */
public class PageInfo {

	private String title;
	private String metaDesc;
	private Integer urlId;

	public PageInfo(String title, String metaDesc, Integer urlId) {
		super();
		this.title = title;
		this.metaDesc = metaDesc;
		this.urlId = urlId;
	}

	public Integer getUrlId() {
		return urlId;
	}

	public void setUrlId(Integer urlId) {
		this.urlId = urlId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getMetaDesc() {
		return metaDesc;
	}

	public void setMetaDesc(String metaDesc) {
		this.metaDesc = metaDesc;
	}

}
