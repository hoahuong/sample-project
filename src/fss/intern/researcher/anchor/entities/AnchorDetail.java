package fss.intern.researcher.anchor.entities;

/**
 * @author Huong mapping object class with anchor table
 */
public class AnchorDetail {

	private Integer id;
	private Integer urlId;
	private String anchorText;
	private String anchorType;
	private String anchorUrl;

	public AnchorDetail(Integer id, String anchorText, String anchorType, String anchorUrl, Integer urlId) {
		super();
		this.id = id;
		this.anchorText = anchorText;
		this.anchorType = anchorType;
		this.anchorUrl = anchorUrl;
		this.urlId = urlId;
	}

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the urlId
	 */
	public Integer getUrlId() {
		return urlId;
	}

	/**
	 * @param urlId
	 *            the urlId to set
	 */
	public void setUrlId(Integer urlId) {
		this.urlId = urlId;
	}

	/**
	 * @return the anchorText
	 */
	public String getAnchorText() {
		return anchorText;
	}

	/**
	 * @param anchorText
	 *            the anchorText to set
	 */
	public void setAnchorText(String anchorText) {
		this.anchorText = anchorText;
	}

	/**
	 * @return the anchorType
	 */
	public String getAnchorType() {
		return anchorType;
	}

	/**
	 * @param anchorType
	 *            the anchorType to set
	 */
	public void setAnchorType(String anchorType) {
		this.anchorType = anchorType;
	}

	/**
	 * @return the anchorUrl
	 */
	public String getAnchorUrl() {
		return anchorUrl;
	}

	/**
	 * @param anchorUrl
	 *            the anchorUrl to set
	 */
	public void setAnchorUrl(String anchorUrl) {
		this.anchorUrl = anchorUrl;
	}

}
