package fss.intern.researcher.anchor.entities;

/**
 * @author Huong mapping object class with url table
 */
public class UrlInfo {

	private Integer id;
	private Integer kwdId;
	private Integer rank;
	private String title;
	private String url;
	private Integer status;

	public static final int DONE = 1;

	public UrlInfo(Integer id, Integer rank, String title, String url, Integer kwdId, int status) {
		super();
		this.id = id;
		this.rank = rank;
		this.title = title;
		this.url = url;
		this.kwdId = kwdId;
		this.status = status;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the kwdId
	 */
	public Integer getKwdId() {
		return kwdId;
	}

	/**
	 * @param kwdId
	 *            the kwdId to set
	 */
	public void setKwdId(Integer kwdId) {
		this.kwdId = kwdId;
	}

	/**
	 * @return the rank
	 */
	public Integer getRank() {
		return rank;
	}

	/**
	 * @param rank
	 *            the rank to set
	 */
	public void setRank(Integer rank) {
		this.rank = rank;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title
	 *            the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * @param url
	 *            the url to set
	 */
	public void setUrl(String url) {
		this.url = url;
	}

}
