package fss.intern.researcher.anchor.services;

import java.util.List;

import fss.intern.researcher.anchor.dao.KeywordDAO;
import fss.intern.researcher.anchor.entities.Keyword;


/**
 * @author Huong 
 * manipulating keyword table
 */
public class KeywordService {

	private KeywordDAO kd;

	public KeywordService(KeywordDAO kd) {
		this.kd = kd;
	}

	public List<Keyword> getKeywordList() {
		return this.kd.getKeywordList();
	}

	public void insert(Keyword kd) {
		this.kd.insert(kd);
	}

}
