package fss.intern.researcher.anchor.services;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import fss.intern.researcher.anchor.dao.AnchorDetailDAO;
import fss.intern.researcher.anchor.dao.KeywordDAO;
import fss.intern.researcher.anchor.dao.UrlListDAO;
import fss.intern.researcher.anchor.entities.AnchorDetail;
import fss.intern.researcher.anchor.entities.Keyword;
import fss.intern.researcher.anchor.entities.PageInfo;
import fss.intern.researcher.anchor.entities.UrlInfo;

/**
 * @author Huong manipulating anchor table and get page content from internet
 */
public class AnchorDetailService {
	private KeywordDAO keyword;
	private UrlListDAO url;
	private AnchorDetailDAO anchorDetail;

	public AnchorDetailService(UrlListDAO url, AnchorDetailDAO anchorDetail, KeywordDAO keyword) {
		this.url = url;
		this.anchorDetail = anchorDetail;
		this.keyword = keyword;
	}

	public PageInfo getPageInfo(int urlId) {
		return anchorDetail.getPageInfo(urlId);
	}

	public List<AnchorDetail> getAllAnchorFromDBLimit(int urlId, int from, int limit) {
		return anchorDetail.getAnchorDetailByLimit(urlId, from, limit);
	}

	public List<AnchorDetail> getAllAnchorDetailFromDB(int urlId) {
		return anchorDetail.getAnchorDetail(urlId);
	}

	public void getAllAnchorDetailFromNet() {

		List<UrlInfo> urlList = this.url.getUrlListAll();

		Iterator<UrlInfo> urlListIterator = urlList.iterator();

		UrlInfo preUi = null;
		while (urlListIterator.hasNext()) {
			UrlInfo ui = urlListIterator.next();
			if (ui.getStatus() != UrlInfo.DONE) {
				// go to IN_PROGRESS_GET_ANCHOR for keyword
				this.keyword.updateStatus(ui.getKwdId(), Keyword.IN_PROGRESS_GET_ANCHOR);
				// get page content (a and img tag)
				this.crawlerUrl(ui.getUrl(), ui.getId());
				// update status of url to 1 if completed to get page content
				this.url.updateStatus(ui.getId(), UrlInfo.DONE);

				// complete to IN_PROGRESS_GET_ANCHOR for keyword
				if (((preUi != null) && (ui.getKwdId() != preUi.getKwdId())) || !urlListIterator.hasNext()) {
					this.keyword.updateStatus(preUi.getKwdId(), Keyword.DONE);
				}
				preUi = ui;
			}

		}

	}

	public void crawlerUrl(String uri, int urlId) {
		Document doc;
		try {
			if (!uri.isEmpty() && uri.startsWith("http")) {
				uri = Jsoup.parse(uri).text();
				System.out.println("urlId " + urlId + " : " + Jsoup.parse(uri).text());
				doc = Jsoup.connect(Jsoup.parse(uri).text())
						.userAgent(
								"Mozilla/5.0 (Windows NT 6.0) AppleWebKit/536.5 (KHTML, like Gecko) Chrome/19.0.1084.46 Safari/536.5")
						.timeout(10000).ignoreHttpErrors(true).get();
				doc.outputSettings().charset("UTF-8");
				Elements aResults = doc.select("a");
				for (Element result : aResults) {
					String text = result.text();
					String url = result.absUrl("href");//Jsoup.parse(result.attr("href")).text();

					System.out.println("result.absUrl(href): "+result.absUrl("href"));
					AnchorDetail ad = new AnchorDetail(null, text, "text", url, urlId);
					anchorDetail.insert(ad);
				}

				// Elements imgResults =
				// doc.select("img[src~=(?i)\\.(png|jpe?g|gif)]");
				Elements imgResults = doc.select("img[src]");
				for (Element result : imgResults) {
					String text = result.attr("alt");
					String url = result.attr("src");

					AnchorDetail ad = new AnchorDetail(null, text, "img", url, urlId);
					anchorDetail.insert(ad);
				}

				String title = doc.select("title").text();
				if (title.isEmpty()) {
					title = doc.select("meta[property=og:title]").attr("content");
				}
				String description = doc.select("meta[name=description]").attr("content");
				if (description.isEmpty()) {
					description = doc.select("meta[property=og:description]").attr("content");
				}
				PageInfo pInfo = new PageInfo(title, description, urlId);
				anchorDetail.insertPageInfo(pInfo);

			}
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
	}
}
