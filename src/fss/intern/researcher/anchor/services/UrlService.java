package fss.intern.researcher.anchor.services;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import fss.intern.researcher.anchor.dao.KeywordDAO;
import fss.intern.researcher.anchor.dao.UrlListDAO;
import fss.intern.researcher.anchor.entities.Keyword;
import fss.intern.researcher.anchor.entities.UrlInfo;

/**
 * @author Huong
 * manipulating url table and get searching content from google page
 */
public class UrlService {

	private KeywordDAO kd;
	private UrlListDAO url;

	public UrlService(KeywordDAO kd, UrlListDAO url) {
		this.kd = kd;
		this.url = url;
	}
	
	public String getUriById(int id) {
		return this.url.getUriById(id);
	}

	public List<UrlInfo> getUrlByKeywordFromDB(int kwdId) {
		return url.getUrlListByKwdId(kwdId);
	}

	public void getUrlByKeywordFromGG() {
		List<Keyword> kdList = this.kd.getKeywordList();

		Iterator<Keyword> it = kdList.iterator();

		while (it.hasNext()) {
			Keyword kw = it.next();
			if (kw.getStatusId() == Keyword.TO_DO) {
				this.crawlerUrl(kw);
				this.kd.updateStatus(kw, Keyword.IN_PROGRESS_GET_LINK_LIST);
			}
		}
	}

	private void crawlerUrl(Keyword kw) {
		String s = kw.getKeyword();
		//s = s.replaceAll(" ", "&nbsp;");
		s = s.replaceAll(" ", "+");

		String uri = "https://www.google.co.jp/search?q=" + s;
		Document doc;
		System.out.println(s);
		try {
			doc = Jsoup.connect(uri).userAgent("Mozilla/5.0 (Windows NT 6.0) AppleWebKit/536.5 (KHTML, like Gecko) Chrome/19.0.1084.46 Safari/536.5")
					.timeout(10000).ignoreHttpErrors(true).get();
			doc.outputSettings().charset("UTF-8");
			Elements results = doc.select("h3.r > a");
			int rank = 1;
			for (Element result : results) {
				String linkHref = result.attr("href");
				if(linkHref.contains("&") && linkHref.length() > 7) {
					linkHref = linkHref.substring(7, linkHref.indexOf("&"));
				}
				String linkText = result.text();
				if (!linkHref.isEmpty() && !linkHref.startsWith("?q")) {
					UrlInfo ui = new UrlInfo(null, rank, linkText, linkHref, kw.getId(), 0);
					url.insert(ui);
				}
				rank++;

				System.out.println("Text:" + linkText + ", URL:" + linkHref);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
