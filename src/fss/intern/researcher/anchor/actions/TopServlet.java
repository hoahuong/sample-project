package fss.intern.researcher.anchor.actions;

import java.io.IOException;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fss.intern.researcher.anchor.dao.KeywordDAOImpl;
import fss.intern.researcher.anchor.entities.Keyword;
import fss.intern.researcher.anchor.services.KeywordService;

/**
 * Servlet implementation class TopServlet
 */
@WebServlet(urlPatterns = { "/top" })
public class TopServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private KeywordService ks = new KeywordService(new KeywordDAOImpl());

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public TopServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		List<Keyword> keywordList = new ArrayList<Keyword>();

		keywordList = this.ks.getKeywordList();

		RequestDispatcher dispatcher = this.getServletContext().getRequestDispatcher("/WEB-INF/views/top.jsp");

		request.setAttribute("keywordList", keywordList);
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8"); // set to insert japanese or vietnamese
		String keyword = request.getParameter("keyword"); // get keyword from user input
		String keywords[] = keyword.split("\\r?\\n");
		for (String k : keywords) {
			if(!k.isEmpty()) {
				Integer status = 0; // status todo of keyword
				Date inputDate = new Date(new java.util.Date().getTime()); // create date
				Keyword kwd = new Keyword(null, k, status, inputDate);
				this.ks.insert(kwd);
			}
		}
		
		
		response.sendRedirect(request.getContextPath() + "/top");
		//doGet(request, response);
	}

}
