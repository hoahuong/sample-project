package fss.intern.researcher.anchor.actions;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fss.intern.researcher.anchor.dao.AnchorDetailDAOImpl;
import fss.intern.researcher.anchor.dao.KeywordDAOImpl;
import fss.intern.researcher.anchor.dao.UrlListDAOImpl;
import fss.intern.researcher.anchor.entities.AnchorDetail;
import fss.intern.researcher.anchor.entities.PageInfo;
import fss.intern.researcher.anchor.services.AnchorDetailService;
import fss.intern.researcher.anchor.services.UrlService;

/**
 * Servlet implementation class AnchorDetailServlet
 */
@WebServlet("/anchor_detail")
public class AnchorDetailServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private AnchorDetailService ads = new AnchorDetailService(new UrlListDAOImpl(), new AnchorDetailDAOImpl(), new KeywordDAOImpl());
	private UrlService us = new UrlService(new KeywordDAOImpl(), new UrlListDAOImpl());   
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AnchorDetailServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		List<AnchorDetail> adList = new ArrayList<AnchorDetail>();
		String urlId = request.getParameter("urlId");
		String numPage = request.getParameter("page");
		int curPage = numPage.isEmpty() ? 0 : Integer.parseInt(numPage)*10;
		String pTitle = "";
		String pDesc = "";
		String curUri = "";
		
		if(!urlId.isEmpty()) {
			curUri = us.getUriById(Integer.parseInt(urlId));
		}
		
		// start get page title and meta
		PageInfo pInfo = this.ads.getPageInfo(Integer.parseInt(urlId));
		if(pInfo != null){
			pTitle = pInfo.getTitle();
			pDesc = pInfo.getMetaDesc();
		}
		// end get page title and meta
		
		// parameter for pagination
		int total = this.ads.getAllAnchorDetailFromDB(Integer.parseInt(urlId)).size();
		int displayLimit = 10; //item number on a page
		int allPageNum = total/10;
		// get anchor detail list from db with limit number
		adList = this.ads.getAllAnchorFromDBLimit(Integer.parseInt(urlId), curPage, displayLimit);
		
		RequestDispatcher dispatcher = this.getServletContext().getRequestDispatcher("/WEB-INF/views/anchor_detail.jsp");

		// set parameter tranfer to jsp file
		request.setAttribute("pTitle", pTitle);
		request.setAttribute("pDesc", pDesc);
		request.setAttribute("allPageNum", allPageNum);
		request.setAttribute("curPage", (curPage < 10) ? 0 : curPage/10);
		request.setAttribute("urlId", urlId);
		request.setAttribute("adList", adList);
		request.setAttribute("curUri", curUri);
		
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
