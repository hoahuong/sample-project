package fss.intern.researcher.anchor.actions;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fss.intern.researcher.anchor.dao.AnchorDetailDAOImpl;
import fss.intern.researcher.anchor.dao.KeywordDAOImpl;
import fss.intern.researcher.anchor.dao.UrlListDAOImpl;
import fss.intern.researcher.anchor.entities.AnchorDetail;
import fss.intern.researcher.anchor.entities.UrlInfo;
import fss.intern.researcher.anchor.services.AnchorDetailService;
import fss.intern.researcher.anchor.services.UrlService;

/**
 * Servlet implementation class UrlListServlet
 */
@WebServlet("/url_list")
public class UrlListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private UrlService us = new UrlService(new KeywordDAOImpl(), new UrlListDAOImpl());

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UrlListServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub

		// get id of done status keyword
		String keywordId = request.getParameter("kwdId");
		// get all url list of google research result from db
		List<UrlInfo> urlList = new ArrayList<UrlInfo>();
		urlList = this.us.getUrlByKeywordFromDB(Integer.parseInt(keywordId));

		RequestDispatcher dispatcher = this.getServletContext().getRequestDispatcher("/WEB-INF/views/url_list.jsp");

		// set parameter to tranfer to jsp 
		request.setAttribute("urlList", urlList);
		request.setAttribute("curPage", 0);
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		 doGet(request, response);
	}

}
