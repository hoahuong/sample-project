import fss.intern.researcher.anchor.dao.KeywordDAOImpl;
import fss.intern.researcher.anchor.dao.UrlListDAOImpl;
import fss.intern.researcher.anchor.services.UrlService;

/**
 * @author Huong
 * This class can run by command prompt or java application to get url of searching result of google
 */

public class GetUrlList {

	public static void main(String[] args) {

		System.out.println("I just test run GetUrlList by command prompt! :)");

		UrlService us = new UrlService(new KeywordDAOImpl(), new UrlListDAOImpl());
		us.getUrlByKeywordFromGG();

	}

}
